#include <iostream>
#include <string>
#include <fstream>
#include <ctype.h>
#include <locale>
using namespace std;

const int delka_souboru = 100;
int main(){
	locale loc;
	char pocet_znaku[10] = "";
	cout << "Pismeno 'k' vzdy ukonci program" << endl;
	cout << "Po kolika znacich zpravu delit - doporuceno 160: ";
	cin >> pocet_znaku;
	cout << endl;
	//ukonceni programu kdyz je zadano 'k'
	if((pocet_znaku[0] == 'k')&&(!pocet_znaku[1])){
		return 0;
	}

	char volba[2] = "";
	cout << "1: Vstupni soubor je sms.txt a vystupni vystup.txt" << endl;
	cout << "2: Vstupni soubor si lze vybrat, vystupni soubor = vystup.txt" << endl;
	cout << "3: Vstupni/vystupni soubor si vyberete sami" << endl;
	cout << "Cokoli jineho - to stejne jak volba 1" << endl;
	cout << "Volba cisla:  ";
	cin >> volba;
	if((volba[0] == 'k')&&(!volba[1])){
		return 0;
	}

	char vstup[delka_souboru] = "sms.txt";

	if((atoi(volba) == 2)||(atoi(volba) == 3)){
		cout << "Nazev vstupniho souboru: ";
		cin >> vstup;
		if((vstup[0] == 'k')&&(!vstup[1])){
			return 0;
		}
	}
	
	ifstream input;
	input.open (vstup);
	if (input.is_open()){
		char vystup[delka_souboru] = "vystup.txt";
		if(atoi(volba) == 3){
			cout << "Nazev vystupniho souboru: ";
			cin >> vystup;
			if((vystup[0] == 'k')&&(!vstup[1])){
				return 0;
				input.close();
			}
		}
		
		ofstream output;
		output.open (vystup);
		if (output.is_open()){
			//oba soubory jsou otevreny bez problemu, program muze zacit
			char text_kazde_zpravy[50] = "";
			cout << endl;
			cout << "Zadejte text, ktery chcete na zacatku kazde zpravy - pri zadani '1' bude text zpravy 'X.sms' (X je poradi cisla zpravy).";
			cout << "Prvni znak je vzdy cislo zpravy." << endl;
			cout << "Text doporucuji ukoncit ':' nebo '-', pripadne '_'" << endl;
			cout << "Text za cislem: ";
			cin >> text_kazde_zpravy;
			if((text_kazde_zpravy[0] == 'k')&&(!text_kazde_zpravy[1])){
				output.close();
				input.close();
				return 0;
			}

			char c;
			input.get(c);
			int i = 0;
			int poradi = 1;
			//nejdrive se znaky ukladaji do stringu slovo a pak az teprve cely slova jsou vypisovana - ve zpravach jsou tak cela slova
			string slovo;
			//cte se vzdy az do konce souboru
			while(!input.eof()){
				//zprava zacina vzdy poradovym cislem
				output << poradi;

				//pokud neni zadan text, je pouzit vychozi
				if(text_kazde_zpravy[0] == '1'){
					output << ".sms:";
				}
				else{
					output << text_kazde_zpravy;
				}

				//je treba pocitat s tim, ze pro delsi zprave bude mit poradi 2 cifry (predpokladam <99 zprav)
				if(poradi > 9){
					i = 7;
				}
				else{
					i = 6;
				}

				//cyklus kontroluje, aby byl dodrzen pocet zadanych znaku v jedne zprave
				for(i; i<atoi(pocet_znaku); i++){
					if(input.eof()){
						break;
					}
					//pismena se nacitaji do stringu dokud neprijde bily znak
					else if(!isspace(c, loc)){
						slovo += c;
					}
					//kdyz prijde bily znak, tak je vypsano slovo a zacne se novy
					else{
						output << slovo;
						output << c;
						slovo = "";
					}
					//nacteni dalsiho znaku
					input.get(c);
				}
				
				//ladici vypis
				/*fprintf(output, "\n\n");
				if(i != 160){
					fprintf(output, "%d", i+1);
				}
				else{
					fprintf(output, "%d", i);
				}*/
				//oddeleni zprav
				output << endl << endl << endl;
				poradi++;
			}
			output.close();
		}
		input.close();
	}
	return 0;

}