### **Description**
SMS cutter

---
### **Technology**
C++

---
### **Year**
2012

---
### **Screenshot**
![console](README/console.png)

### **Example**
#### input:
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in ex pretium, volutpat mi id, dignissim augue. Donec nec felis vitae est consectetur finibus. Sed eget risus elit. Etiam eget varius elit. Donec ullamcorper gravida lorem, eu facilisis dolor efficitur eu. Nunc nisi metus, finibus vel nulla et, viverra iaculis leo. Phasellus volutpat nibh vel ante pulvinar, quis aliquet nibh ornare. Fusce nec odio velit. Sed posuere volutpat porttitor. Nam facilisis eget erat at tristique. In hac habitasse platea dictumst.

Quisque a dolor vel ante auctor lobortis. Vivamus quis diam et lectus vestibulum viverra sed non libero. Maecenas auctor egestas iaculis. Aenean risus turpis, commodo auctor orci non, ullamcorper sollicitudin nunc. Cras sed varius magna. Morbi eget sodales lorem. Cras pulvinar iaculis quam, a vulputate leo. Proin dapibus turpis metus, id ultrices ex auctor quis. Nullam vel nisi hendrerit, congue est at, ultrices lacus. Phasellus fringilla viverra augue, ac pharetra turpis laoreet at. Donec eu ultrices lorem, vitae interdum dolor. Mauris consectetur eu ex id luctus. Vivamus maximus sem risus, id convallis nisl faucibus sed. Donec mattis ante vitae quam posuere, eget sagittis augue scelerisque.

Sed vitae sem a ipsum sodales fermentum eget ut ante. Aliquam lobortis purus sollicitudin, tristique mi et, pellentesque turpis. Vestibulum rhoncus, justo id faucibus tempor, eros enim malesuada nunc, sed mattis nisi urna vitae diam. Donec imperdiet eget lectus nec egestas. In a lorem nec nunc dictum commodo sed in massa. Donec semper ac tellus non vehicula. Suspendisse erat enim, fringilla id nulla vitae, feugiat dapibus sapien. Suspendisse nec tempor ligula. Nullam tempus dui turpis, quis imperdiet libero tincidunt laoreet. Pellentesque tincidunt velit in blandit fermentum.

Quisque vestibulum neque eu facilisis posuere. Nulla consectetur sapien et erat efficitur, id vulputate nisl porttitor. In ullamcorper odio vitae ante condimentum gravida. Sed finibus facilisis enim, non blandit diam cursus ut. Donec id velit urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris dictum dictum ex sit amet posuere. Vestibulum posuere risus ut risus suscipit, eget iaculis justo facilisis. Nam sed ullamcorper elit. Aenean nec felis id ipsum bibendum luctus at at sapien. Suspendisse viverra, lorem a euismod varius, velit erat tempus dolor, sed dapibus enim eros quis lectus. Quisque facilisis fringilla magna nec pellentesque. Etiam sapien turpis, tempus sed dui quis, dapibus commodo lacus. Morbi vel velit sed sem viverra cursus.

Fusce eros arcu, dapibus eu mauris eu, ultrices malesuada est. Ut quis metus vehicula, lobortis sapien ut, vulputate purus. Aliquam erat volutpat. Vivamus ut nisi ultricies, ultricies metus vitae, iaculis est. Praesent pretium elit eu tortor suscipit, in ultricies quam dignissim. Maecenas neque neque, tincidunt nec ipsum non, bibendum egestas tortor. Duis nulla justo, maximus id suscipit non, rutrum a ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum fermentum tristique sollicitudin. Sed pharetra nunc nec viverra maximus. Sed blandit ipsum at dictum facilisis. Duis ac semper ligula. Fusce blandit tincidunt erat vitae sollicitudin. Proin convallis felis commodo sagittis malesuada.

#### output:
1.sms:Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in ex pretium, volutpat mi id, dignissim augue. Donec nec felis vitae est consectetur finib


2.sms:us. Sed eget risus elit. Etiam eget varius elit. Donec ullamcorper gravida lorem, eu facilisis dolor efficitur eu. Nunc nisi metus, finibus vel nulla et, 


3.sms:viverra iaculis leo. Phasellus volutpat nibh vel ante pulvinar, quis aliquet nibh ornare. Fusce nec odio velit. Sed posuere volutpat porttitor. Nam facili


4.sms:sis eget erat at tristique. In hac habitasse platea dictumst.

Quisque a dolor vel ante auctor lobortis. Vivamus quis diam et lectus vestibulum viverra se


5.sms:d non libero. Maecenas auctor egestas iaculis. Aenean risus turpis, commodo auctor orci non, ullamcorper sollicitudin nunc. Cras sed varius magna. Morbi e


6.sms:get sodales lorem. Cras pulvinar iaculis quam, a vulputate leo. Proin dapibus turpis metus, id ultrices ex auctor quis. Nullam vel nisi hendrerit, congue 


7.sms:est at, ultrices lacus. Phasellus fringilla viverra augue, ac pharetra turpis laoreet at. Donec eu ultrices lorem, vitae interdum dolor. Mauris consectetu


8.sms:r eu ex id luctus. Vivamus maximus sem risus, id convallis nisl faucibus sed. Donec mattis ante vitae quam posuere, eget sagittis augue scelerisque.

Sed 


9.sms:vitae sem a ipsum sodales fermentum eget ut ante. Aliquam lobortis purus sollicitudin, tristique mi et, pellentesque turpis. Vestibulum rhoncus, justo id 


10.sms:faucibus tempor, eros enim malesuada nunc, sed mattis nisi urna vitae diam. Donec imperdiet eget lectus nec egestas. In a lorem nec nunc dictum commodo se


11.sms:d in massa. Donec semper ac tellus non vehicula. Suspendisse erat enim, fringilla id nulla vitae, feugiat dapibus sapien. Suspendisse nec tempor ligula. N


12.sms:ullam tempus dui turpis, quis imperdiet libero tincidunt laoreet. Pellentesque tincidunt velit in blandit fermentum.

Quisque vestibulum neque eu facilisi


13.sms:s posuere. Nulla consectetur sapien et erat efficitur, id vulputate nisl porttitor. In ullamcorper odio vitae ante condimentum gravida. Sed finibus facili


14.sms:sis enim, non blandit diam cursus ut. Donec id velit urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris 


15.sms:dictum dictum ex sit amet posuere. Vestibulum posuere risus ut risus suscipit, eget iaculis justo facilisis. Nam sed ullamcorper elit. Aenean nec felis id


16.sms: ipsum bibendum luctus at at sapien. Suspendisse viverra, lorem a euismod varius, velit erat tempus dolor, sed dapibus enim eros quis lectus. Quisque faci


17.sms:lisis fringilla magna nec pellentesque. Etiam sapien turpis, tempus sed dui quis, dapibus commodo lacus. Morbi vel velit sed sem viverra cursus.

Fusce er


18.sms:os arcu, dapibus eu mauris eu, ultrices malesuada est. Ut quis metus vehicula, lobortis sapien ut, vulputate purus. Aliquam erat volutpat. Vivamus ut nisi


19.sms: ultricies, ultricies metus vitae, iaculis est. Praesent pretium elit eu tortor suscipit, in ultricies quam dignissim. Maecenas neque neque, tincidunt nec


20.sms: ipsum non, bibendum egestas tortor. Duis nulla justo, maximus id suscipit non, rutrum a ipsum. Vestibulum ante ipsum primis in faucibus orci luctus et ul


21.sms:trices posuere cubilia Curae; Vestibulum fermentum tristique sollicitudin. Sed pharetra nunc nec viverra maximus. Sed blandit ipsum at dictum facilisis. D


22.sms:uis ac semper ligula. Fusce blandit tincidunt erat vitae sollicitudin. Proin convallis felis commodo sagittis malesuada.

