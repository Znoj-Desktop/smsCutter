#include <iostream>
#include <string>
#include <fstream>
#include <ctype.h>
#include <locale>
using namespace std;

const int delka_souboru = 100;
int main(){
	locale loc;
	char pocet_znaku[10] = "";
	cout << "Pismeno 'k' vzdy ukonci program" << endl;
	cout << "Po kolika znacich zpravu delit - doporuceno 160: ";
	cin >> pocet_znaku;
	cout << endl;
	//ukonceni programu kdyz je zadano 'k'
	if((pocet_znaku[0] == 'k')&&(!pocet_znaku[1])){
		return 0;
	}

	char volba[2] = "";
	cout << "1: Vstupni soubor je sms.txt a vystupni vystup.txt" << endl;
	cout << "2: Vstupni soubor si lze vybrat, vystupni soubor = vystup.txt" << endl;
	cout << "3: Vstupni/vystupni soubor si vyberete sami" << endl;
	cout << "4: Zadani vstupu z klavesnice a vystup do \"vystup.txt\"" << endl;
	cout << "5: Zadani vstupu z klavesnice a vystupni soubor si muzete vybrat" << endl;
	cout << "Cokoli jineho - to stejne jak volba 1" << endl;
	cout << "Volba cisla:  ";
	cin >> volba;
	if((volba[0] == 'k')&&(!volba[1])){
		return 0;
	}

	//priznak vstupu z klavesnice a podminka pro jeho nastaveni
	bool klavesnice = 0;
	if((atoi(volba) == 4)||(atoi(volba) == 5)){
		klavesnice = 1;
	}

	char vstup[delka_souboru] = "sms.txt";

	if((atoi(volba) == 2)||(atoi(volba) == 3)){
		cout << "Nazev vstupniho souboru: ";
		cin >> vstup;
		if((vstup[0] == 'k')&&(!vstup[1])){
			return 0;
		}
	}
	ifstream input;
	if(!klavesnice){
		input.open (vstup);
	}
	if (input.is_open() || klavesnice){
		char vystup[delka_souboru] = "vystup.txt";
		if((atoi(volba) == 3) || (atoi(volba) == 5)){
			cout << endl << "Nazev vystupniho souboru: ";
			cin >> vystup;
			if((vystup[0] == 'k')&&(!vstup[1])){
				return 0;
				input.close();
			}
		}
			
		ofstream output;		
		output.open (vystup);
		if (output.is_open()){
			//oba soubory jsou otevreny bez problemu, program muze zacit
			char text_kazde_zpravy[50] = "";
			cout << endl;
			cout << "Zadejte text, ktery chcete na zacatku kazde zpravy - pri zadani '1' bude text zpravy 'X.sms' (X je poradi cisla zpravy).";
			cout << "Prvni znak je vzdy cislo zpravy." << endl;
			cout << "Text doporucuji ukoncit ':' nebo '-', pripadne '_'" << endl;
			cout << "Text za cislem: ";
			cin >> text_kazde_zpravy;
			if((text_kazde_zpravy[0] == 'k')&&(!text_kazde_zpravy[1])){
				output.close();
				input.close();
				return 0;
			}

			char c;
			//pokud se zapisuje primo do okna, pak se bude text zalohovat do souboru
			ofstream zaloha;
			if(!klavesnice){
				input.get(c);
			}
			else{
				cout << endl << "Zadejte text Vasi sms zpravy. Psani ukoncite znakem '$'" << endl;
				cout << "Znak '$' se dela kombinaci klaves 'Alt Gr + 'u s krouzkem'' nebo 'Ctrl + Alt + 'u s krouzkem''" << endl;
				cout << "Zaloha zpravy bude ulozena v souboru \"zaloha.txt\"" << endl;
				cout << "Zprava: ";
				//otevreni souboru pro vytvoreni zalohy vstupu
				zaloha.open("zaloha.txt");
				c = getchar();
			}
			int i = 0;
			int poradi = 1;
			int delka_zacatku = 0;
			//nejdrive se znaky ukladaji do stringu slovo a pak az teprve cely slova jsou vypisovana - ve zpravach jsou tak cela slova
			string slovo;
			//cte se vzdy az do konce souboru
			while(!input.eof() && (c != '$')){
				//zprava zacina vzdy poradovym cislem
				output << poradi;

				//pokud neni zadan text, je pouzit vychozi
				if((text_kazde_zpravy[0] == '1')&&(!text_kazde_zpravy[1])){
					output << ".sms:";
					delka_zacatku = 5;
				}
				else{
					output << text_kazde_zpravy;
					delka_zacatku = strlen(text_kazde_zpravy);
				}

				//je treba pocitat s tim, ze pro delsi zprave bude mit poradi 2 cifry (predpokladam <999 zprav)
				if(poradi > 99){
					delka_zacatku+=3;
				}
				else if(poradi > 9){
					delka_zacatku+=2;
				}
				else{
					delka_zacatku+=1;
				}
				i = delka_zacatku;

				//cyklus kontroluje, aby byl dodrzen pocet zadanych znaku v jedne zprave
				for(i; i<atoi(pocet_znaku); i++){
					//kdyz prijde konec souboru, tak program konci
					if(input.eof() || (c == '$')){
						//pro zapsani posledniho slova je tato podminka nutna
						if(slovo != ""){
							output << slovo;
						}
						break;
					}
					//pismena se nacitaji do stringu dokud neprijde bily znak, aby bylo zapsano vzdy cele slovo
					if(!isspace(c, loc)){
						slovo += c;
						//pokud je slovo delsi nez text zpravy, tak se rozdeli
						if(slovo.length()+delka_zacatku >= atoi(pocet_znaku)){
							output << slovo;
							slovo = "";
						}
					}
					//kdyz prijde bily znak, tak je vypsano slovo a zacne se slovo novy
					else{
						//zmezi slovy se zapise vzdy jen jeden bily znak, takze sms nezacina nikdy mezerou
						if(!slovo.empty()){
							output << slovo;
							output << ' ';
							slovo = "";
						}
						//pokud se nic nezapise, pak se ani nepocitaji znaky :-)
						else{
							i--;
						}
					}
					//nacteni dalsiho znaku
					if(!klavesnice){
						input.get(c);
					}
					else{
						c = getchar();
						//zaroven zapis do zalohy
						if(zaloha.is_open()){
							zaloha << (char) c;
						}
					}
				}
				
				//oddeleni zprav
				output << endl << endl << endl;
				poradi++;
			}
			output.close();
			if(klavesnice){
				char volba[2] = "";
				cout << endl << "0: Vypsat vystup" << endl;
				cout << "1 nebo cokoli jinyho: konec programu" << endl;
				cout << "Volba cisla:  ";
				cin >> volba;
				if((atoi(volba) == 0)){
					int length;
					char * buffer;
					cout << endl;

					ifstream read_file;
					read_file.open(vystup);
					
					// get length of file:
					read_file.seekg (0, ios::end);
	  			    length = read_file.tellg();
				    read_file.seekg (0, ios::beg);

				    // allocate memory:
				    buffer = new char [length];

				    // read data as a block:
				    read_file.read (buffer,length);
				    read_file.close();

				    cout.write (buffer,length);

				    delete[] buffer;
				    system("PAUSE");
				}
			}
			
		}
		input.close();
	}
	return 0;

}